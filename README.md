# Open Mobility indicator Jupyter notebook "Average speeds of public transport sections"

## description : [indicator.yml file](https://gitlab.com/open-mobility-indicators/indicators/sections/-/blob/main/indicator.yaml)

This indicator calculates the average speed between consecutive stop areas

forked from [jupyter notebook template](https://gitlab.com/open-mobility-indicators/indicator-templates/jupyter-notebook)

[Install locally using a venv or Docker, Build and Use (download, compute)](https://gitlab.com/open-mobility-indicators/indicator-templates/jupyter-notebook/-/blob/main/README.md#jupyter-notebook-indicator-template)  

